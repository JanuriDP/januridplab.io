---
layout: page
title: About
permalink: /about/
---

<p class="message">
  Semua tulisan yang ada disini adalah kacamata atau pandangan atau persepsi atau opini semata, yaitu mata gua.
</p>

### Abstrak
Ide bikin media ini di mulai pada hari Jum'at, 07 Desember 2018.  Cerita lanjutnya nanti ada di post __Intro__ karena di page ini gua cuma mau tulis kenapa/tentang apa media ini dibuat. Di blog ini gua akan menulis, ya menulis, semua  pandangan atau persepsi atau opini gua terhadap kehidupan, iya cuy _kehidupan_. Kalo ada yg ga setuju atau ada perbedaan di antara kita hahaha nanti akan gua kasih kolom komentar di tiap post ya, jadi silakan komen apa persetujuan/perbedaan antara kita hahaha... kalo kurang nanti gua bikin forum online atau kalo kurang juga nanti kita kopdar aja wkwkw riding bareng boleh tuh mantap

### Tujuan
Ok, pasti bakal muncul _apa sih tujuan lu bikin tulisan begini?_ Gua bikin beginian salah satu tujuan simple-nya ya dokumentasi. Gua bisa nulis apapun tentang kehidupan yg gua jalani, semua bakal gua tulis mulai dari diri gua sendiri, seperti apa sifat gua, kepribadian gua, kepribadian orang, banyak ketemu orang baru, sifat orang baru, cara gua untuk menghadapi situasi atau kondisi, dan lain sebagainya haha ya bisa juga menjadi salah satu tempat untuk berekspresi dan juga melatih ngetik gua dengan mata menatap monitor dan 10 jari tetap ngetik wkwkwk walaupun ga 10 jari gua bekerja semua sih, tapi lumayanlah nambahin speed ngetik

### Batasan
Tentu ada batasnya dong biar ga kelewatan. Sebisa mungkin gua akan tulis se-normal mungkin walau banyak _wkwkwk_-nya dan ketawanya yg dimana pada saat ngetik pun gua kaga ketawa cuma gua tulis aja _hahaha_ ujung kalimat. Jikalau ada dari gua yang melebihi batas normal atau wajar, gua siap banget nerima tegoran, saran bahkan kritik malah gua mohon banget untuk di ingetin :pray:

Penulisan dalam blog ini adalah murni skill gua hahaha jadi bisa keliatan deh kata kata gua yg baik/kaga dari cara penulisannya atau gaya bahasanya atau bahkan gua itu orangnya kaya gimana hahaha, ini gaya gua kalo ga suka ya jangan marah cuy :stuck_out_tongue_winking_eye:

Ada 1 lagi tulisan gua yg lebih ke arah teknis tentang komputer, yaitu:

* [JanuriDP's Blog](http://blog.januridp.com/)

Blog itu juga mau gua lanjut lagi nulisnya tentang komputer, semoga bisa dan lancar aaamiiin...

### Tech-Stack

Nah berikut ini adalah teknologi yg dipakai di blog ini, simple kok

* [Jekyll](http://jekyllrb.com)
* [Theme](https://github.com/poole/lanyon)
* [GitHub Pages](https://pages.github.com)


Thanks for visiting and happy reading ya cuy!
